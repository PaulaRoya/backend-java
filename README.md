# README

\|---------------------------|

## Microservice 2
\|---------------------------|

---


## MAIEUTICS PROJECT

---

### Logotype

![Maieutics Logotype Design](https://i.postimg.cc/9fsVWnnW/Team.png "Maieutics Logotype")

---

### Development Team
* **Santiago Melo | santiagomelo48@gmail.com**
* **Paula Roya | morenitoroya@gmail.com**
* **Oliver G | oliverdereverde2@gmail.com**
* **Sebastián Restrepo | sebastian.respo@gmail.com**
* **Isaac P | isaacpd98@gmail.com**
  
---

### Mockups Web Design
click [here](https://drive.google.com/file/d/1b6hqFvE_OxN9tU9y6qaYyPRDtlfB1aZ5/view?usp=sharing) to view AppWeb **Maieutics** mokcups.

---

### Microservice Architecture

![Microservice Architecture Image](https://i.postimg.cc/t45Y4p39/Dise-o-Arquitectura-Microservicios.png "Microservice Architecture Design")

---

### Sprints Evidence Document

Clic [here](https://docs.google.com/spreadsheets/d/1zsdT9UehbGMenQiCR9zfsmsiA83vlhvGl6W3H7Y2kFI/edit#gid=0) to view sprints evidence.

---



### Work Flow

The **Maieutics** Development Team work on a workflow platforma called **Jira**. Where 5 sprints will be carried out for the project.

Our project link is [WorkFlow Jira](https://maieutics.atlassian.net/jira/software/projects/MAIEUT/boards/1/backlog)

[![Jira Logo Workflow](https://logos-marcas.com/wp-content/uploads/2021/03/Jira-Logo.png "Jira Logo")](https://maieutics.atlassian.net/jira/software/projects/MAIEUT/boards/1/backlog)

---

### BitBucket Repository

The Maieutics Development Team, works with the Bitbucket platform for project repository.
to view this, clic [here](https://bitbucket.org/PaulaRoya/4a-docs/src/RM-MASTER/).


[![Bitbucket Logo repository](https://download.logo.wine/logo/Bitbucket/Bitbucket-Logo.wine.png "Bitbucket logo")](https://bitbucket.org/PaulaRoya/4a-docs/src/RM-MASTER/)

---